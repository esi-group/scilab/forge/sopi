sopi_begin;
x1          = sopi_var(1);                  // Optimisation variables
x2          = sopi_var(1);
x3          = sopi_var(1);
x4          = sopi_var(1);
fun         = x1  + 2*x2 + 3*x3 + 4*x4;     // Objective function
constraints = x1 + x2 + x3 + x4 == 1 &...   // Constraints
              x1 + x3-3*x4 == 1/2 &...
              x1 >= 0 &...
              x2 >= 0 &...
              x3 >= 0 &...
              x4 >= 0;
problem     = sopi_min(fun,constraints);    // Problem creation
problem.solve("sopiLP");                            // Resolution
xopt        = problem.getOptValue();        // Extraction of the solution which is 7/8,0,0,1/8
//sopi_minimise(fun,constraints)
sopi_end;

