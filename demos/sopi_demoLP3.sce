sopi_begin;
x = sopi_var(1);
a = [-3;1;2];
b = [3;0;-3];
sopi_minimise(max(-3*x + 3 ,1*x, 2*x-3),"sopiLP");    // internal simplex
sopi_end;
disp("x = "+string(x))                      // x = 0.75
