// Copyright 2015 - Pierre Vuillemin
//
// Date of creation: 3 nov. 2015
//
help_lang_dir = get_absolute_file_path('builder_help.sce');

tbx_build_help(TOOLBOX_TITLE, help_lang_dir);

clear help_lang_dir;
