// Copyright 2015 - Pierre Vuillemin
//
// Date of creation: 4 nov. 2015
sopi_loadConstants()
sopi_begin;
n     = 3;
x     = sopi_var(n);
A     = ones(n,n);
z     = A * x;
// Test on the nature of z
assert_checkfalse(z.isElementary); // z is a composite variable
assert_checktrue(z.isLinear); // z is linear
assert_checkfalse(z.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z.isConvex); // z is not convex (strictly speaking)
// Test on the content of z
assert_checkequal(size(z),[n,1]);
assert_checkequal(z.child.operator,LEFT_LINEAR_MAPPING); // the last transformation must be +
assert_checkequal(z.child.arg(1),A); 
assert_checkequal(z.child.arg(2).id,x.id); 
//
B     = ones(n,n);
z2    = B*z;
// Test on the nature of z
assert_checkfalse(z2.isElementary); // z is a composite variable
assert_checktrue(z2.isLinear); // z is linear
assert_checkfalse(z2.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z2.isConvex); // z is not convex (strictly speaking)
// Test on the content of z
assert_checkequal(size(z2),[n,1]);
assert_checkequal(z2.child.operator,LEFT_LINEAR_MAPPING); // the last transformation must be +
assert_checkequal(z2.child.arg(1),B*A); 
assert_checkequal(z2.child.arg(2).id,x.id); 
// Test distribution
b  = ones(n,1);
z3 = B*(A*x + b);
// Test on the nature of z
assert_checkfalse(z3.isElementary); // z is a composite variable
assert_checktrue(z3.isLinear); // z is linear
assert_checkfalse(z3.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z3.isConvex); // z is not convex (strictly speaking)
// Test on the content of z
assert_checkequal(size(z3),[n,1]);
assert_checkequal(z3.child.operator,"+"); // the last transformation must be +
assert_checkequal(z3.child.arg(1).child.operator,LEFT_LINEAR_MAPPING); 
assert_checkequal(z3.child.arg(1).child.arg(1),B*A);
assert_checkequal(z3.child.arg(1).child.arg(2).id,x.id);
assert_checkequal(z3.child.arg(2).child.arg(1),B*b); 
//
x2 = sopi_var(n,n);
z4 = A*(x2*B);
assert_checkfalse(z4.isElementary); // z is a composite variable
assert_checktrue(z4.isLinear); // z is linear
assert_checkfalse(z4.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z4.isConvex); // z is not convex (strictly speaking)
//
assert_checkequal(size(z4),[n,n]);
assert_checkequal(z4.child.operator,LEFT_LINEAR_MAPPING);
assert_checkequal(z4.child.arg(1),A); 
assert_checkequal(z4.child.arg(2).child.operator,RIGHT_LINEAR_MAPPING); 
assert_checkequal(z4.child.arg(2).child.arg(1),B); 
assert_checkequal(z4.child.arg(2).child.arg(2).id,x2.id); 
