// Copyright 2015 - Pierre Vuillemin
//
// Date of creation: 3 nov. 2015
//
sopi_begin;
n     = 3;
x     = sopi_var(3);
b     = ones(n,1);
z     = x + b;
// Test on the nature of z
assert_checkfalse(z.isElementary); // z is a composite variable
assert_checktrue(z.isLinear); // z is linear
assert_checkfalse(z.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z.isConvex); // z is not convex (strictly speaking)
// Test on the content of z
assert_checkequal(size(z),[3,1]);
assert_checkequal(z.child.operator,"+"); // the last transformation must be +
assert_checkequal(z.child.arg(1).id,x.id); 
assert_checkequal(z.child.arg(2).child.arg(1),b); 
// 
z2 = z + b;
assert_checkequal(z2.child.operator,"+");
assert_checkequal(z2.child.arg(1).id,x.id);
assert_checkequal(z2.child.arg(2).child.arg(1),b+b);
//
y     = sopi_var(3);
z3    = x + y + b;
// Test on the nature of z3
assert_checkfalse(z3.isElementary); // z is a composite variable
assert_checktrue(z3.isLinear); // z is linear
assert_checkfalse(z3.isCPWA); // z is not cpwa (strictly speaking)
assert_checkfalse(z3.isConvex); // z is not convex (strictly speaking)
// Test on the content of z3
assert_checkequal(size(z3),[3,1]);
assert_checkequal(z3.child.operator,"+"); // the last transformation must be +
assert_checkequal(z3.child.arg(1).id,x.id); 
assert_checkequal(z3.child.arg(2).id,y.id); 
assert_checkequal(z3.child.arg(3).child.arg(1),b); 
