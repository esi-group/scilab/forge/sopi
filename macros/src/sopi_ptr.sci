// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// SOPIPTR CREATION
// ============================================================================

// sopi_ptr ....................................................................
// creates a new pointer towards a given field in sopiNameSpace. It firstly 
// finds an empty space in the list sopiNameSpace(target) which becomes the id
// of the couple pointer/variable.
function p = sopi_ptr(target)
   id  = sopi_findFreeId(target);
   p   = sopi_newPtr(target,id);
endfunction

// sopi_newPtr .................................................................
// creates an empty pointer object.
function p = sopi_newPtr(target,id)
   objType  = "sopiPtr";
   objAtt   = list("id"    ,     id,...
                   "target", target);
   p        = sopi_makeObject(objType,objAtt);
endfunction

// ============================================================================
// TOOLS FUNCTIONS OVERLOADING
// ============================================================================

// disp .......................................................................
// calls the display function of the variable/constraint/problem corresponding
// to a given pointer.
function %sopiPtr_p(p)
   disp(sopi_getVar(p))
endfunction

// size ........................................................................
// calls the size function of the variable/constraint/problem corresponding to a
// given pointer.
function [out1,out2] = %sopiPtr_size(p,opt)
   if argn(2) == 1 then
      opt = 12;
   end
   if argn(1) == 2 &  opt ~=12 then
      error("incompatible number of ouput args")
   end
   if p.isVar then
      var = sopi_getVar(p);
      s   = size(var,opt);
   end
   if argn(1) == 2 then
      out1 = s(1);
      out2 = s(2);
   else
      out1 = s;
   end
endfunction

// ============================================================================
// FUNCTIONS OVERLOADING
// ============================================================================

// norm ........................................................................
function out = %sopiPtr_norm(p,opt)
   sopi_expectPtrToVar(p);
   if argn(2) == 1 then
      normType            = 2;
   else
      normType            = opt;
      if and(normType~=[1,2,%inf]) then
         error(string(opt)+"-norm not supported by sopi");
      end
   end
   var     = sopi_getVar(p);
   newVar  = norm(var,normType);
   out     = sopi_storeVar(newVar);
   sopi_cleanTmpVar(p);
endfunction

// abs .........................................................................
function out = %sopiPtr_abs(p)
   sopi_expectPtrToVar(p);
   var     = sopi_getVar(p);
   newVar  = abs(var);
   out     = sopi_storeVar(newVar);
   sopi_cleanTmpVar(p);
endfunction

// max .........................................................................
function out = %sopiPtr_max(varargin)
   sopi_expectPtrToVar(varargin(:));
   [m,n]   = size(varargin(1));
   varList = list();
   for i = 1:length(varargin)
      if i >= 2 then
         if or(size(varargin(i)) ~= [m,n]) then
            error("inconsistent sizes in max function")
         end
      end
      varList($+1) = sopi_getVar(varargin(i));
   end
   newVar  = sopi_max(varList)
   out     = sopi_storeVar(newVar);
   sopi_cleanTmpVar(varargin(:));
endfunction

// power .......................................................................
function out = %sopiPtr_p_s(p,pow)
   sopi_expectPtrToVar(p);
   var     = sopi_getVar(p);
   newVar  = var^pow;
   out     = sopi_storeVar(newVar);
   sopi_cleanTmpVar(p);
endfunction

// sum .........................................................................
function out = %sopiPtr_sum(p,opt)
   if argn(2) < 2 then
      opt = 12;
   end
   if and( opt ~= [1,2,12]) then
      error("wrong dimension for sum direction")
   end
   sopi_expectPtrToVar(p);
   var      = sopi_getVar(p);
   newVar   = sum(var,opt);
   out      = sopi_storeVar(newVar);
   sopi_cleanTmpVar(p);
endfunction

// trace .......................................................................
function out = %sopiPtr_trace(p)
   sopi_expectPtrToVar(p);
   var      = sopi_getVar(p);
   newVar   = trace(var);
   out      = sopi_storeVar(newVar);
   sopi_cleanTmpVar(p);
endfunction

// ============================================================================
// EXTRACTION AND INSERTION OVERLOADING
// ============================================================================

// field extraction ............................................................
function out = %sopiPtr_e(varargin)
   if length(varargin) == 2 then
   
      out = sopi_extractOneField(varargin(1),varargin(2));
   elseif length(varargin) == 3 then
      out = sopi_extractTwoFields(varargin(1),varargin(2),varargin(3));
   else
      error("Invalid extraction")
   end
endfunction

// sopi_extractOneField ........................................................
// extracts one field from the pointer p.
function out = sopi_extractOneField(i,p)
   if i == "isVar" then
      out = p.target == "var";
   elseif i == "isCst" then
      out = p.target == "cst";
   elseif i == "setBinary" then
      sopi_setVar(p,"type","binary");
      out = %t;
   else
      var = sopi_getVar(p);
      out = var(i);
   end
endfunction

// sopi_extractTwoFields .......................................................
// extracts two fields from the pointer p.
function out = sopi_extractTwoFields(i,j,p)
   var = sopi_getVar(p);
   out = var(i,j);
endfunction

// ============================================================================
// OPERATORS OVERLOADING
// ============================================================================

// sopiPtr + sopiPtr ...........................................................
function out = %sopiPtr_a_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2)
   sopi_print(5,"Performing sopiVar + sopiVar.\n")
   var1     = sopi_getVar(p1);
   var2     = sopi_getVar(p2);
   newVar   = var1 + var2;
   out      = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p1,p2);
endfunction

// A * sopiPtr .................................................................
function out = %s_m_sopiPtr(A,p)
   sopi_expectPtrToVar(p)
   sopi_print(5,"Performing matrix*sopiVar.\n")
   var     = sopi_getVar(p);
   newVar  = A*var;
   out     = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p);
endfunction

// sopiPtr * A .................................................................
function out = %sopiPtr_m_s(p,A)
   sopi_expectPtrToVar(p)
   sopi_print(5,"Performing sopiVar*matrix.\n")
   var     = sopi_getVar(p);
   newVar  = var*A;
   out     = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p);
endfunction

// b + sopiPtr .................................................................
function out = %s_a_sopiPtr(b,p)
   out = p+b;
endfunction

// sopiPtr + b .................................................................
function out = %sopiPtr_a_s(p,b)
   sopi_expectPtrToVar(p)
   if isempty(b) then
      out = p;
   else
      sopi_print(5,"Performing sopiVar + Matrix.\n")
      var     = sopi_getVar(p);
      newVar  = var + b;
      out     = sopi_storeVar(newVar);
//      sopi_cleanTmpVar(p);
   end
endfunction

// sopiPtr - b .................................................................
function out = %sopiPtr_s_s(p,b)
   sopi_expectPtrToVar(p)
   sopi_print(5,"Performing sopiVar - Matrix.\n")
   var     = sopi_getVar(p);
   newVar  = var + (-1)*b;
   out     = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p);
endfunction

// -sopiPtr ....................................................................
function out = %sopiPtr_s(p)
   sopi_expectPtrToVar(p)
   sopi_print(5,"Performing -sopiVar = (-1)*sopiVar.\n")
   out = (-1)*p;
endfunction

// sopiPtr + sopiPtr ...........................................................
function out = %sopiPtr_s_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2)
   sopi_print(5,"Performing sopiVar - sopiVar.\n")
   out = p1 + (-p2);
endfunction

// sopiPtr' ....................................................................
function out = %sopiPtr_t(p)
   sopi_expectPtrToVar(p)
   sopi_print(5,"Performing sopiVar''.\n")
   var     = sopi_getVar(p);
   newVar  = var';
   out     = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p);
endfunction

// sopiPtr * sopiPtr ...........................................................
function out = %sopiPtr_m_sopiPtr(p1,p2)
   sopi_expectPtrToVar(p1,p2);
   var1    = sopi_getVar(p1);
   var2    = sopi_getVar(p2);
   newVar  = var1*var2;
   out     = sopi_storeVar(newVar);
//   sopi_cleanTmpVar(p1,p2);
endfunction
