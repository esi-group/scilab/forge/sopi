// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

//
// ============================================================================
// GENERAL TOOLS
// ============================================================================

// sopi_mode ...................................................................
// enables to modify global parameters of sopi such as its verbosity
function sopi_mode(varargin)
   global sopiNameSpace
   for sopiMode = varargin
      select sopiMode
      case "verbose"
         sopiNameSpace.mode(1) = evstr(varargin(2));
         sopi_print(0,"Sopi verbosity set to %d.\n",sopiNameSpace.mode(1))
      case "globalConstraints"
         sopiNameSpace.mode(2) = %t;
         sopi_print(0,"Global constraints mode enabled\n")
      end
   end
endfunction

// sopi_isVerbose ..............................................................
// tests whether the verbose mode is activated
function verbosity = sopi_verbosity()
   global sopiNameSpace
   verbosity = sopiNameSpace.mode(1);
endfunction

// sopi_constraintsAreGlobal ...................................................
// tests whether the global constraints mode is activated
function globalCst = sopi_constraintsAreGlobal()
   global sopiNameSpace
   globalCst = sopiNameSpace.mode(2);
endfunction

// sopi_print ..................................................................
// prints a given string if the verbose mode is activated
function sopi_print(printLvl,strList,varargin)
   [message, tagStr] = sopi_extractTags(strList);
   message           = " " + message;
   if sopi_verbosity()>= printLvl then
      if printLvl == -1 then
         tagStr = tagStr + "[WARNING]";
      end
      mprintf(tagStr + message, varargin(:));
   end
endfunction

// sopi_extractTags ............................................................
// separate the message from the tags given a string or matrix of strings.
function [message, tagStr] = sopi_extractTags(strList)
   strList = ["sopi",strList];
   if  size(strList,2) > 1 then
      message     = strList($);
      tags        = strList(1:$-1);
      tagStr      = "[" + strcat(tags,"][") + "]";
   else
      message  = strList;
      tagStr   = [];
   end
endfunction

// sopi_optimReport ............................................................
function report = sopi_optimReport(info)
   report = [];
   report = [report,"Elapsed time : "+string(info.elapsedTime),"CPU time : "+string(info.CPUTime)]
endfunction

// sopi_ptrAreEquals ...........................................................
// tests whether two sopiPtr are equals
function out = sopi_ptrAreEquals(p1,p2)
   bothPtr         = typeof(p1) == "sopiPtr" & typeof(p2) =="sopiPtr";
   if bothPtr then
      out = p1.target == p2.target & p1.id == p2.id;
   else
      out = %f;
   end
endfunction

// sopi_varAreEquals ...........................................................
// tests whether two sopiVar represent the same thing.
function ans = sopi_varAreEquals(var1,var2)
   if typeof(var1) == "sopiPtr" then
      var1 = sopi_getVar(var1);
   end
   if typeof(var2) == "sopiPtr" then
      var2 = sopi_getVar(var2);
   end
   sopi_loadConstants()
   if var1.isElementary & var2.isElementary then
      ans = var1.id == var2.id;
   else
      if var1.child.operator ~= var2.child.operator then
         ans = %f
      else
         select var1.child.operator
            // same last transformation
         case SUM
            if length(var1.child.arg) == length(var2.child.arg) then
               ans = %t;
               J = 1:length(var2.child.arg);
               for i = 1:length(var1.child.arg)
                  vi          = var1.child.arg(i);
                  matchFound  = %f;
                  for j = J
                     vj    = var2.child.arg(j);
                     ansj  = sopi_varAreEquals(vi,vj);
                     if ansj then
                        J(J == j)   = [];
                        matchFound  = %t;
                        break;
                     end
                  end
                  if ~matchFound then
                     ans = %f;
                     break;
                  end
               end
            else
               ans = %f
            end
         case LEFT_LINEAR_MAPPING
            ans = and(var1.child.arg(1) == var2.child.arg(1)) & sopi_varAreEquals(var1.child.arg(2),var2.child.arg(2));
         case RIGHT_LINEAR_MAPPING
            ans = and(var1.child.arg(1) == var2.child.arg(1)) & sopi_varAreEquals(var1.child.arg(2),var2.child.arg(2));
         case CONSTANT
            ans = and(var1.child.arg(1) == var2.child.arg(1));
         case FUN
            if var1.child.arg(1) == var2.child.arg(1) then
               // TODO : a finir
               error("not yet supported")
            else
               ans = %f
            end  
         end
      end
   end
endfunction

// sopi_checkSizesCoherence ....................................................
// tests the coherence given two elements e1 and e2 and an operation. It returns
// the resulting size.
function newSize = sopi_checkSizesCoherence(e1,e2,operation)
   mn1 = size(e1);
   mn2 = size(e2);
   select operation
   case TIMES
      ok      = isscalar(e1) | isscalar(e2) | mn1(2) == mn2(1);
      if isscalar(e1) then
         newSize = mn2;
      elseif isscalar(e2) then
         newSize = mn1;
      else
         newSize = [mn1(1),mn2(2)];
      end
   case SUM
      ok      = isscalar(e1) | isscalar(e2) | and(mn1 == mn2);
      if isscalar(e1) then
         newSize = mn2;
      elseif isscalar(e2) then
         newSize = mn1;
      else
         newSize = mn1;
      end
   case CONSTRAINT
      ok      = and(mn1 == mn2);
      newSize = mn1;
   end

   if ~ok then
      error("Incompatible sizes : "+string(mn1(1))+"x"+string(mn1(2))+ " " + operation + " " + string(mn2(1))+"x"+string(mn2(2)))
   end
endfunction

// sopi_getVarDependency .......................................................
// goes through the transformation tree of a variable to determine the variables
// involved in this tree.
function ptrList = sopi_getVarDependency(var)
   sopi_loadConstants()
   ptrList = list();
   if var.isElementary then
      ptrList(1) = sopi_newPtr("var",var.id);
   else
      select var.child.operator
      case LEFT_LINEAR_MAPPING
         ptrList  = var.child.arg(2).dependsOf;
      case RIGHT_LINEAR_MAPPING
         ptrList  = var.child.arg(2).dependsOf;
      case SUM
         for v = var.child.arg
            ptrList = sopi_union(ptrList,v.dependsOf);
         end
      case FUN
         if var.child.arg(1) == MAX then
            for v = var.child.arg(2)
               ptrList = sopi_union(ptrList,v.dependsOf);
            end
         else
            ptrList = var.child.arg(2).dependsOf;
         end
      end
   end
endfunction

// sopi_union ..................................................................
// performs the union of two lists of pointers (without redundancy).
function out = sopi_union(ptrList1,ptrList2)
   candidateList   = lstcat(ptrList1,ptrList2);
   out             = list();
   for i = 1:length(candidateList)
      ptrVari            = candidateList(i);
      isAlreadyInOut  = %f;
      for j = 1:length(out)
         if sopi_ptrAreEquals(ptrVari,out(j)) then
            isAlreadyInOut = %t;
            break;
         end
      end
      if ~isAlreadyInOut then
         out($+1) = ptrVari;
      end
   end
endfunction

// ============================================================================
// VARIABLE STORAGE
// ============================================================================

// sopi_findFreeID .............................................................
// finds an empty space in the global list target.
function idx = sopi_findFreeId(target)
   global sopiNameSpace
   li          = sopiNameSpace(target);
   idxFound    = %f;
   for i = 1:length(li)
      if isempty(li(i)) then
         idxFound    = %t;
         idx         = i;
         break;
      end
   end
   if ~idxFound then
      idx             = length(li) + 1;
   end
endfunction

// sopi_store ..................................................................
// stores the element e in the global list target.
function ptr = sopi_store(target,e)
   global sopiNameSpace
   ptr                             = sopi_ptr(target);
   e.id                            = ptr.id;
   sopiNameSpace(target)(ptr.id)   = e;
endfunction

// sopi_getVar .................................................................
// loads the element (variable, constraint, etc.) corresponding to the pointer p.
function e = sopi_getVar(p)
   global sopiNameSpace;
   if length(sopiNameSpace(p.target)) < p.id then
      error("Variable does not exists! (getVar)")
   else
      e = sopiNameSpace(p.target)(p.id);
      // TODO: empty variable check?
   end
endfunction

// sopi_setVar .................................................................
// sets the given field of a given elements to a new value.
function sopi_setVar(p,field,fieldContent)
   if typeof(p) ~= "sopiPtr" then
      error("Expecting sopiPtr and not "+typeof(p)+" to setVar.")
   end
   var         = sopi_getVar(p)
   var(field)  = fieldContent;
   sopi_replace(p,var);
endfunction

// sopi_replace ................................................................
// replaces the var given by its pointer p by newVar.
function sopi_replace(p,newVar)
   global sopiNameSpace
   sopiNameSpace(p.target)(p.id) = newVar;
endfunction

// sopi_clean ..................................................................
// erases the variable given by its pointer.
function sopi_clean(p)
   global sopiNameSpace
   sopiNameSpace(p.target)(p.id) = [];
endfunction

// sopi_cleanTmpVar ............................................................
// erases the variables which are temporary given their pointers.
// TODO : empêcher de supprimer une variable encore utilisée.
function sopi_cleanTmpVar(varargin)
//   for i = 1:length(varargin)
//      if varargin(i).isTemporary then
//         sopi_clean(varargin(i))
//      end
//   end
endfunction

// sopi_retrievePtr ............................................................
// builds the pointer given a variable.
function ptr = sopi_retrievePtr(var)
   if var.isElementary then
      ptr = sopi_newPtr("var",var.id);
   else
      ptr = sopi_store("var",var);
   end
endfunction

// sopi_getAllConstraints ......................................................
// extracts all the constraints from the sopiNameSpace.
function cstPtr = sopi_getAllConstraints()
   global sopiNameSpace
   cstPtr = list();
   for c = sopiNameSpace.cst
      cstPtr($+1) = sopi_newPtr("cst",c.id);
   end
endfunction

// sopi_parseOptimOptions ......................................................
// fills the structure opt with the fields from default that do not exist in 
// opt.
function out = sopi_parseOptimOptions(opt,default)
   out     = default;
   if isempty(opt) then
      return;
   end
   done    = %f;
   i       = 1;
   while ~done
      key         = opt(i);
      val         = opt(i+1);
      if isfield(out,key) then
         out(key)    = val;
      else
         sopi_print(0,"Unknown option %s, ignoring.\n",key);
      end
   end
endfunction

// ============================================================================
// DEVELOPMENT TOOLS
// ============================================================================

// sopi_loadConstants ..........................................................
// loads predefined constants (such as strings) used throughout the sopi code.
// The predefined constants are stored in the global variable sopiNameSpace. 
// They are initialised when sopi_begin is called.
function sopi_loadConstants()
   global sopiNameSpace
   constants   = sopiNameSpace.constants;
   outputArgs  = constants(:,1);
   value       = constants(:,2);
   execstr("[" + strcat(outputArgs,",")+"] = resume(" + strcat(value,",") + ");");
endfunction

// sopi_makeObject .............................................................
// is the unified interface for objects creation in sopi.
// It requires the type of the object (string) and its attributes together with
// their initial values represented as a list : 
// objAtt = list("attribute1",attribute1InitialValue,...)
function obj = sopi_makeObject(objType,objAtt)
   attributesList = list(objAtt(1:2:$));
   initialValues  = list(objAtt(2:2:$));
   // Convert list of attributes to array of attributes
   attributes     = [];
   for a = attributesList
      attributes = [attributes,a]
   end
   // Build the object
   obj            = mlist([objType,attributes])
   // Assigne the initial values
   for i = 1:length(initialValues)
      field       = attributesList(i);
      obj(field)  = initialValues(i);
   end
endfunction

// sopi_expectPtrToVar ........................................................
// tests whether its arguments are sopiPtr pointing towards sopiVar. 
// If not, it produces an error.
function sopi_expectPtrToVar(varargin)
   for e = varargin
      if typeof(e) ~= "sopiPtr" then
         error("Expecting sopiPtr instead of "+typeof(e))
      else
         if ~e.isVar then
            error("Expecting sopiPtr pointing towards sopiVar")
         end
      end
   end
endfunction

// sopi_expectPtrToCst .........................................................
// tests whether its arguments are sopiPtr pointing towards sopiCst. 
// If not, it produces an error.
function sopi_expectPtrToCst(varargin)
   for e = varargin
      if typeof(e) ~= "sopiPtr" then
         error("Expecting sopiPtr instead of "+typeof(e))
      else
         if ~e.isCst then
            error("Expecting sopiPtr pointing towards sopiCst")
         end
      end
   end
endfunction

// sopi_whoIsSopiPtr ...........................................................
// scans the current variables to determine which ones are sopiPtr objects or 
// list of sopiPtr objects.
function [ptrList,ptrName] = sopi_whoIsSopiPtr()
   ptrList = list();
   ptrName = list();
   varList = who_user(%f);
   for i = 1:size(varList,1)
      varName = varList(i);
      execstr("var = "+varName);
      if  typeof(var) == "sopiPtr" then
         ptrList($+1) = var;
         ptrName($+1) = varName;
      end
   end
endfunction

// sopi_testIfInstalled ........................................................
// tests whether an external module is available or not.
function sopi_testIfInstalled(module,errorMessage)
   if ~isdef(module) then
      error(errorMessage)
   end
endfunction

