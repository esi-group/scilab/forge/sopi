// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// sopi_begin ..................................................................
// initialises a new namespace for sopi and also cleans the remaining sopiPtr.
function sopi_begin()
   global sopiNameSpace
   previousprot = funcprot(1)
   funcprot(0)
   sopiNameSpace           = mlist(["sopiNameSpace","var","cst","pb","mode","constants"]);
   sopiNameSpace.var       = list();
   sopiNameSpace.cst       = list();
   sopiNameSpace.pb        = list();
   sopiNameSpace.constants =  ["LEFT_LINEAR_MAPPING","""llm""",
                               "RIGHT_LINEAR_MAPPING","""rlm""",
                               "SUM","""+""",
                               "FUN","""fun""",
                               "TRANSPOSE","""transpose""",
                               "CONSTANT","""constant""",
                               "QUADRATIC","""quad""",
                               "REAL","""real""",
                               "BINARY","""binary""",
                               "INTEGER","""integer""",
                               "LEFT","""l""",
                               "RIGHT","""r""",
                               "LINEAR_MAPPING","""lm""",
                               "LESSER_THAN","""<""",
                               "GREATER_THAN",""">""",
                               "EQUALS","""=""",
                               "TIMES","""*""",
                               "ABS","""abs""",
                               "NORM","""norm""",
                               "MAX","""max""",
                               "POW","""pow""",
                               "CONSTRAINT","""<>""",
                               "POSITIVE_DEF","""pd""",
                               "POSITIVE_SEMIDEF","""psd""",
                               "NEGATIVE_DEF","""nd""",
                               "NEGATIVE_SEMIDEF","""nsd""",
                               "INDEFINITE","""i""",
                               "CONVEX","""convex""",
                               "CONCAVE","""concave""",
                               "NONLINEAR","""nonlinear"""
                              ]
   sopiNameSpace.mode      = list(-1,%f);
   exStr                   = sopi_clearSopiPtrInstruction(); // clear remaining sopiPtr var
   execstr(exStr);
endfunction

// sopi_end ....................................................................
// cleans the namespace and the sopiPtr variables in the workspace.
function sopi_end()
   clearglobal sopiNameSpace
   exStr = sopi_clearSopiPtrInstruction()
   funcprot(1)
   execstr(exStr);
endfunction

// sopi_clearSopiPtrInstruction ................................................
// returns the string to be executed at the end of a routine to clean the 
// existing sopiPtr variables.
function exStr = sopi_clearSopiPtrInstruction()
   exStr   = [];
   argStr  = [];
   varList = who_user(%f);
   for i = 1:size(varList,1)
      varName = varList(i);
      execstr("var = "+varName);
      if  typeof(var) == "sopiPtr" then
         mustBeCleaned = %t;
      elseif typeof(var) == "list" then
         if typeof(var(1)) == "sopiPtr" then
            mustBeCleaned = %t;
         else
            mustBeCleaned = %f;
         end
      else
         mustBeCleaned = %f;
      end
      if mustBeCleaned then
         exStr   = [exStr,varName];
         argStr  = [argStr,"clear"];
      end
   end
   if ~isempty(exStr) then
      exStr = "[" + strcat(exStr,",") + "] = resume("+strcat(argStr,",")+");";
   end
endfunction

// sopi_about ..................................................................
// displays some informations about sopi.
function sopi_about()
   mprintf("+----------------------------------------------------------------+\n")
   mprintf("|                         ABOUT SOPI                             |\n")
   mprintf("|            Scilab Optimisation Problem Interpreter             |\n")
   mprintf("+----------------------------------------------------------------+\n")
   mprintf("| Site    : http://sopi.pierre-vuillemin.fr                      |\n")
   mprintf("| Authors : Pierre Vuillemin (contact@pierre-vuillemin.fr)       |\n")
   mprintf("|           with the help of Simon Vernhes                       |\n")
   mprintf("| Licence : CECILL (see www.cecill.info)                         |\n")
   mprintf("+----------------------------------------------------------------+\n")
endfunction
