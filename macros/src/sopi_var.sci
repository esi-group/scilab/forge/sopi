// Copyright 2015 - Pierre Vuillemin
// License CECILL
//

// ============================================================================
// SOPIVAR
// ============================================================================

// sop_var .....................................................................
// creates a sopiVar object of given sizes, stores it inside sopiNameSpace and
// returns a pointer pointing towards its position in sopiNameSpace.
function  varPtr = sopi_var(varSize1,varargin)
   if length(varargin)>0 then
      s = [varSize1,varargin(1)];
   else
      s = [varSize1,1];
   end
   sopi_print(2,"Creation of a sopiVar of size %dx%d.\n",s(1),s(2));
   var                 = sopi_newVar();
   var.size            = s;
   var.isTemporary     = %f;
   var.class           = sopi_newFunClass("affine",s(1),s(2));
   varPtr              = sopi_storeVar(var);
endfunction

// sopi_newVar ................................................................ 
// creates an empty sopiVar object
function var = sopi_newVar()
   objType  = "sopiVar";
   objAtt = list("name"          ,                 "",...
                  "id"           ,                 "",...
                  "isTemporary"  ,                 %t,...
                  "class"        , sopi_newFunClass(),...
                  "size"         ,                 [],...
                  "type"         ,             "real",...
                  "child"        ,     sopi_newTree(),...
                  "setName"      ,                 []);
   var      = sopi_makeObject(objType,objAtt);
endfunction

// sopi_newTree ................................................................
// creates an empty transformation tree object. This tree is used to save the 
// transformations applied to a sopiVar.
function t = sopi_newTree()
   objType  = "sopiTre";
   objAtt   = list("operator" ,     "",...
                   "arg"      , list());
   t        = sopi_makeObject(objType,objAtt);
endfunction

// sopi_addSpecificVarFields ...................................................
// adds fields corresponding to functions to a sopiVar given its associated
// pointer. The function calls are wrapped in other functions containing the id 
// of the sopiVar. It enables to pass the variable as argument while writting
// var.function(args).
function sopi_addSpecificVarFields(p)
   funStart = ["id = " + string(p.id)+";";...
               "ptr = sopi_newPtr(""var"",id);"];
   deff("foo(name)",[funStart; 
                     "sopi_setVar(ptr,""name"",name);"]);
   sopi_setVar(p,"setName",foo);
endfunction

// sopi_storeVar ...............................................................
// stores a given sopiVar in sopiNameSpace and returns a pointer towards the 
// variable.
function ptr = sopi_storeVar(newVar)
   ptr = sopi_store("var",newVar);
   sopi_addSpecificVarFields(ptr)
endfunction

// sopi_testVar ................................................................
// tests some properties of a given sopiVar.
function out = sopi_testVar(test,var)
   sopi_loadConstants();
   select test
   case "isReal"
      out = var.type == REAL;
   case "isBinary"
      out = var.type == BINARY;
   case "isInteger"
      out = var.type == INTEGER;
   case "isElementary"
      out = isempty(var.child.operator);
   case "isTemporary"
      out = var.isTemporary;
   case "isLinear"
      out = and(var.class.curvature == 1);
   case "isConvex"
      out = and(var.class.curvature == 2);
   case "isCPWA"
      if var.isConvex then
         out = sopi_checkIfPWA(var);
      else
         out = %f;
      end
   else
      error("invalid test for variables")
   end
endfunction

// sopi_checkIfPWA .............................................................
// tests whether a given variable is piecewise affine (stricly, i.e. not linear)
function ans = sopi_checkIfPWA(var)
   sopi_loadConstants()
   if var.isElementary  then
      // if a variable is elementary, then it is affine, therefore it is not, 
      // strictly speaking, piecewise affine.
      ans = %f;
   end
   select var.child.operator
   case SUM
      ans = %t;
      for e = var.child.arg
         // a sum of pwa functions is pwa, 
         // a sum of pwa and linear functions is pwa
         ans = ans & (sopi_checkIfPWA(e) | e.isLinear);
      end
      // ans is true if at least one of the element of the sum is pwa and the other
      // linear or pwa
   case LEFT_LINEAR_MAPPING 
      // a linear mapping of a pwa function is pwa
      ans = sopi_checkIfPWA(var.child.arg(2));
   case RIGHT_LINEAR_MAPPING
      ans = sopi_checkIfPWA(var.child.arg(2));
   case FUN
      if var.child.arg(1) == NORM then
         if or(var.child.arg(3) == [%inf,1]) then
            if var.child.arg(2).isLinear | sopi_checkIfPWA(var.child.arg(2)) then
               ans = %t
            else
               ans = %f;
            end
         end
      elseif var.child.arg(1) == MAX then
         ans = %t;
         for e = var.child.arg(2)
            ans = ans & e.isLinear | sopi_checkIfPWA(e);
         end
      elseif var.child.arg(1) == ABS then
         if var.child.arg(2).isLinear | sopi_checkIfPWA(var.child.arg(2)) then
            ans = %t
         else
            ans = %f;
         end
      end
   end
endfunction

// ============================================================================
// SPECIFIC SOPIVAR
// ============================================================================

// sopi_scalar .................................................................
// enables to quickly declare several scalar sopiVar
function sopi_scalar(varargin)
   varPtr  = list();
   str     = ""
   strOut  = "";
   for i = 1:length(varargin) // TODO : a simplifier.
      varPtr(i)   = sopi_var(1);
      str         = str + "varPtr(" + string(i) + ")";
      strOut      = strOut + varargin(i);
      if i < length(varargin)
         str     = str + ",";
         strOut  = strOut + ",";
      end
   end
   execstr("[" + strOut + "]=resume(" + str + ")");
endfunction

// sopi_set ....................................................................
// creates a set (i.e. a list) of sopiVar of given sizes. Useful for MPC.
function varSet = sopi_set(cardinal,m,n)
   if argn(2)<3 then
      // if not told otherwise, vector variables are created
      n = 1;
   end
   varSet = list();
   for i = 1:cardinal
      varSet(i) = sopi_var(m,n);
   end
endfunction

// sopi_newConstantVar .........................................................
// creates an empty sopiVar representing a constant. It is used when adding a
// sopiVar and a constant matrix.
function constantVar = sopi_newConstantVar(b)
   sopi_loadConstants();
   constantVar                 = sopi_newVar()
   constantVar.size            = size(b);
   constantVar.class           = sopi_newFunClass("constant",size(b,1),size(b,2));
   constantVar.child.operator  = CONSTANT;
   constantVar.child.arg(1)    = b;
endfunction

// ============================================================================
// TOOLS FUNCTIONS OVERLOADING
// ============================================================================

// disp ........................................................................
// handles the display of sopiVar in the scilab terminal.
function %sopiVar_p(var)
   sizeStr = " " + string(size(var,1))+"x"+string(size(var,2));
   if ~isempty(var.name) then
      nameStr = var.name + " : ";
   else
      nameStr = "";
   end
   disp(nameStr+ var.type + sizeStr+ " sopiVar ");
endfunction

// size ........................................................................
// returns the size of a given sopiVar.
function [out1,out2] = %sopiVar_size(var,opt)
   if argn(2) == 1 then
      // by default, get both sizes.
      opt = 12;
   end
   if argn(1) == 2 &  opt ~=12 then
      // two output arguments but only one dimension is asked.
      error("incompatible number of ouput args")
   end
   select opt
   case 1
      s = var.size(1);
   case 2
      s = var.size(2);
   case 12
      s = [var.size(1),var.size(2)];
   end
   if argn(1) == 2 then
      // two output arguments, each dimension is returned in one output argument
      out1 = s(1);
      out2 = s(2);
   else
      // one output argument, the dimension(s) are returned as a vector
      out1 = s;
   end
endfunction

// ============================================================================
// EXTRACTION AND INSERTION OVERLOADING
// ============================================================================

// field extraction ............................................................
function out = %sopiVar_e(varargin)
   if length(varargin) == 2 then
      var         = varargin(2);
      i           = varargin(1);
      if typeof(i) == "string" then
         validFields = ["name","id","isTemporary","nature","size","type","child"];         
         if part(i,1:2) == "is" then
            out     = sopi_testVar(i,var);
         elseif i == "dependsOf" then
            out     = sopi_getVarDependency(var);
         elseif or(i == validFields) then
            out     = var(i);
         else
            error("unkown field for sopiVar")
         end
      else
         newVar  = sopi_extractElement(var,i);
         out     = sopi_storeVar(newVar);
      end
   elseif length(varargin) == 3 then
      i        = varargin(1);
      j        = varargin(2);
      var      = varargin(3);
      newVar   = sopi_extractElement(var,i,j);
      out      = sopi_storeVar(newVar);
   end
endfunction

// sopi_extractElement .........................................................
// builds the linear transformation needed to extract given elements from a 
// sopiVar.
function out = sopi_extractElement(varargin)
   var      = varargin(1);
   [m, n]   = size(var);
   // row extraction
   i        = varargin(2);
   Im       = eye(m,m);
   Ei       = Im(i,:)
   if Ei == Im then
      // all the rows are required, i.e. x(:,columnID)
      out = var;
   else
      // only some rows are required
      out = Ei * var;
   end
   if length(varargin) == 3 then
      // column extraction
      j  = varargin(3);
      In = eye(n,n);
      Ej = In(:,j);
      if Ej ~= In then
         // only some columns are required
         out  = out * Ej;
      end
   end
endfunction

// ============================================================================
// MATH FUNCTIONS OVERLOADING
// ============================================================================

// norm ........................................................................
// computes the norm of a sopiVar.
// TODO: change 1-norm for sum(abs(.)) and inf -norm for max(abs(.)) ?
function newVar = %sopiVar_norm(var,normType)
   sopi_loadConstants();
   newVar                  = sopi_newVar();
   newVar.size             = [1,1];
   newVar.child.operator   = FUN;
   newVar.child.arg(1)     = NORM;
   newVar.child.arg(2)     = var;
   newVar.child.arg(3)     = normType;
   newVar.class            = sopi_applyCompositionRules(var.class,2,newVar.size);
endfunction

// abs .........................................................................
// computes the absolute value of a sopiVar.
function newVar = %sopiVar_abs(var)
   sopi_loadConstants();  
   newVar                  = sopi_newVar();
   newVar.size             = size(var);
   newVar.child.operator   = FUN;
   newVar.child.arg(1)     = ABS;
   newVar.child.arg(2)     = var;
   newVar.class            = sopi_applyCompositionRules(var.class,2,newVar.size);   
endfunction

// max .........................................................................
// computes the pointwise maximum of a list of sopiVar.
function newVar = sopi_max(varList)
   sopi_loadConstants();   
   newVar                  = sopi_newVar();
   newVar.size             = [1,1];
   newVar.child.operator   = FUN;
   newVar.child.arg(1)     = MAX;
   newVar.child.arg(2)     = varList;
   //    newVar.child.arg(3)     = var2;
   newVar.class            = sopi_newFunClass("affine",newVar.size(1),newVar.size(2));
   for i = 1:length(varList)
      newVar.class        = sopi_applyPointwiseMaxRules(newVar.class,varList(i).class);
   end
endfunction

// power .......................................................................
// computes the power of a sopiVar.
function newVar = %sopiVar_p_s(var,pow)
   sopi_loadConstants();   
   [m,n] = size(var);
   if m~=n then
      error("Power only for square variables");
   end
   if pow == 1 then
      newVar = var;
   else
      newVar                  = sopi_newVar();
      newVar.size             = [m,n];
      newVar.child.operator   = FUN;
      newVar.child.arg(1)     = POW;
      newVar.child.arg(2)     = var;
      newVar.child.arg(3)     = pow;
      if pow > 0 & pow < 1 then
         newVar.class = sopi_applyCompositionRules(var.class,-2,newVar.size);
      elseif pow > 1 & pmodulo(pow,2) == 0 then
         newVar.class = sopi_applyCompositionRules(var.class,2,newVar.size);
      else
         newVar.class = sopi_applyCompositionRules(var.class,3,newVar.size);
      end
   end
endfunction

// sum .........................................................................
// computes the sum of the elements of a sopiVar
function newVar = %sopiVar_sum(var,opt)
   sopi_loadConstants();
   [m,n] = size(var);
   if opt == 1 then
      // sum along the row dimension
      if m == 1 then
         // only one row, return the var
         newVar = var;
      else
         newVar = ones(1,m) * var;
      end
   elseif opt == 2 then
      // sum along the column dimension
      if n == 1 then
         // only one column, return the var
         newVar = var;
      else
         newVar = var * ones(n,1);
      end
   elseif opt == 12 then
      // sum along both dimensions
      newVar   = sum(sum(var,2),1);
   end
endfunction

// trace .......................................................................
// computes the trace of a sopiVar
function newVar = %sopiVar_trace(var)
   if isscalar(var) then
      newVar = var;
   elseif sopi_isSquare(var) then
      n        = size(var);
      newVar   = var(1,1);
      for i = 2:n
         newVar = newVar + var(i,i);
      end
   else
      error("Trace can only be applied to square matrices.")
   end
endfunction

// ============================================================================
// CONSTRAINTS CREATION
// ============================================================================

// sopi_defineConstraint .......................................................
function constraint = sopi_defineConstraint(lhs,operator,rhs)
   // checking constraint coherence
   if sopi_isMatrix(lhs) then
      if operator == GREATER_THAN | operator == LESSER_THAN then
         // Matrix inequalities are understood in the sense of positive semi
         // definite matrices
         if sopi_isSquare(lhs) then
            if isscalar(rhs) then
               rhs = rhs * speye(size(lhs,1),size(lhs,2));
            end
         else
            error("Rectangular matrix inequalities are not authorised, use vec(.) for element-wise.")
         end
      else
         // Matrix equalities are understood element-wise
         if isscalar(rhs) then
            rhs = rhs * ones(size(lhs,1),size(lhs,2));
         end
      end
   else
      // Vector inequalities are understood element-wise
      if isscalar(rhs) then
         rhs = rhs * ones(size(lhs,1),size(lhs,2))
      end
   end
   newSize           = sopi_checkSizesCoherence(lhs,rhs,CONSTRAINT);
   // constraint creation
   constraint        = sopi_newCst();
   constraint.type   = operator;
   constraint.lhs    = lhs;
   constraint.rhs    = rhs;
   constraint.class  = sopi_determineConstraintClass(lhs,operator,rhs);
endfunction

// sopi_determineConstraintClass ...............................................
// determines the nature of the constraint
// TODO : check that, not sure
function class = sopi_determineConstraintClass(lhs,operator,rhs)
   // a constraint is non-linear, unless it is not
   class = 3;
   if lhs.isLinear then
      // the lhs is linear then the constraint is linear/affine
      class = 1;
   elseif lhs.isConvex then
      // the lhs is convex
      if operator == LESSER_THAN then
         // convex constraint
         class = 2;
      elseif operator == GREATER_THAN then
         // concave constraint
         class = -2;
      end
   elseif lhs.isConcave then
      // the lhs is concave
      if operator == GREATER_THAN then
         // convex constraint
         class = 2;
      elseif operator == LESSER_THAN then
         // concave constraint
         class = -2;
      end
   end
endfunction

// ============================================================================
// OPERATION OPERATORS OVERLOADING
// ============================================================================

// sopiVar + sopiVar ...........................................................
// computes the addition of two sopiVar.
function newVar = %sopiVar_a_sopiVar(var1,var2)
   sopi_loadConstants();
   // checking sizes
   newSize                 = sopi_checkSizesCoherence(var1,var2,SUM);
   if isscalar(var1) & ~isscalar(var2) then
      var1 = ones(newSize(1),newSize(2)) * var1;
   elseif isscalar(var2) & ~isscalar(var1)  then
      var2 = ones(newSize(1),newSize(2)) * var2;
   end
   // creating new variable
   newVar                  = sopi_newVar();
   newVar.size             = newSize;
   newVar.child.operator   = SUM;
   terms1                  = sopi_expandSum(var1);
   terms2                  = sopi_expandSum(var2);
   newVar.child.arg        = lstcat(terms1,terms2);
   newClass                = newVar.child.arg(1).class;
   for i = 2:length(newVar.child.arg)
      //        disp(size(newVar.child.arg(i)))
      newClass = sopi_applyAdditionRules(newClass,newVar.child.arg(i).class);
   end
   newVar.class           = newClass;
endfunction

// sopi_expandSum ..............................................................
// develops a sopiVar if it is already formed by a sum of sopiVar.
function terms = sopi_expandSum(var)
   if var.child.operator == SUM then
      terms = var.child.arg;
   else
      terms = list(var);
   end
endfunction

// A * sopiVar .................................................................
// performs a left matrix multiplication between a matrix and a sopiVar.
function newVar = %s_m_sopiVar(A,var)
   sopi_loadConstants();
   // check sizes
   newSize                 = sopi_checkSizesCoherence(A,var,TIMES);
   newVar                  = sopi_newVar();
   newVar.size             = newSize;
   [child,class]           = sopi_applyLinearMapping(A,var,LEFT);
   newVar.child            = child;
   newVar.class            = class
endfunction

// sopiVar * A .................................................................
// performs a right matrix multiplication between a sopiVar and a matrix.
function newVar = %sopiVar_m_s(var,A)
   if isscalar(A) then
      newVar          = A*var;
   else
      sopi_loadConstants();
      // check sizes
      newSize         = sopi_checkSizesCoherence(var,A,TIMES);
      newVar          = sopi_newVar();
      newVar.size     = newSize;
      [child,class]   = sopi_applyLinearMapping(A,var,RIGHT);
      newVar.child    = child;
      newVar.class    = class;
   end
endfunction

// sopi_applyLinearMapping .....................................................
// expands the multiplication of a sopiVar by a matrix by propagating the 
// multiplication along the transformation tree of the sopiVar.
function [t,class] = sopi_applyLinearMapping(A,var,side)
   // determine new size
   if isscalar(A) then
      [mA,nA] = size(var);
      A       = A*speye(mA,mA);
   else
      [mA,nA] = size(A);
   end
   if side == LEFT then
      m = mA;
      n = size(var,2);
   else
      m = size(var,1);
      n = nA;
   end
   // Propagate linear mapping in the tree
   t = sopi_newTree();
   select var.child.operator
   case LEFT_LINEAR_MAPPING
      class           = sopi_applyLinearMappingRules(var.child.arg(2).class,A)
      t.operator      = side + LINEAR_MAPPING;
      if side == LEFT then
         t.arg(1)    = A*var.child.arg(1);
         t.arg(2)    = var.child.arg(2);
      else
         t.arg(1)    = A;
         t.arg(2)    = var;
      end
   case RIGHT_LINEAR_MAPPING
      class           = sopi_applyLinearMappingRules(var.child.arg(2).class,A)
      t.operator      = side + LINEAR_MAPPING;
      if side == RIGHT then
         t.arg(1)    = var.child.arg(1)*A;
         t.arg(2)    = var.child.arg(2);
      else
         t.arg(1)    = A;
         t.arg(2)    = var;
      end
   case SUM
      t.operator          = SUM;
      class               = sopi_newFunClass("constant",m,n);
      for i = 1:length(var.child.arg)
         termi           = var.child.arg(i);
         newVar          = sopi_newVar();
         [child, cla]    = sopi_applyLinearMapping(A,termi,side);
         newVar.child    = child;
         newVar.class    = cla;
         newVar.size     = [m,n];
         class           = sopi_applyAdditionRules(class,newVar.class);
         t.arg(i)        = newVar;
      end
   case CONSTANT
      t.operator      = CONSTANT;
      if side == LEFT then
         class       = sopi_newFunClass("constant",m,n);
         t.arg(1)    = A * var.child.arg(1);
      else
         class       = sopi_newFunClass("constant",m,n);
         t.arg(1)    = var.child.arg(1) * A;
      end
   else
      class       = sopi_applyLinearMappingRules(var.class,A)
      t.operator  = side + LINEAR_MAPPING;
      t.arg(1)    = A;
      t.arg(2)    = var;
   end
endfunction

// sopiVar + b .................................................................
// add a sopiVar to a matrix.
function newVar = %sopiVar_a_s(var,b)
   sopi_loadConstants();
   //
   if isempty(b) then
      newVar = var;
      return;
   end
   // checking sizes
   newSize                 = sopi_checkSizesCoherence(var,b,"+");
   if isscalar(b) then
      b = b*ones(newSize(1),newSize(2));
   end
   if var.child.operator == SUM then
      newVar              = var;
      constantValueFound  = %f;
      for i = 1:length(var.child.arg)
         childVar = var.child.arg(i);
         if childVar.child.operator == CONSTANT then
            newVar.child.arg(i).child.arg(1)    = childVar.child.arg(1) + b;
            constantValueFound                  = %t;
            break;
         end
      end
      if ~constantValueFound then
         constantVar             = sopi_newConstantVar(b);
         newVar.child.arg($+1)   = constantVar;
      end
   else
      constantVar                = sopi_newConstantVar(b);
      newVar                     = sopi_newVar();
      newVar.child.operator      = SUM;
      newVar.child.arg(1)        = var;
      newVar.child.arg(2)        = constantVar;
      newClass                   = constantVar.class;
      newClass                   = sopi_applyAdditionRules(newClass,newVar.child.arg(1).class);
      newVar.class               = newClass;
   end
   newVar.size             = newSize;
endfunction

// b + sopiVar .................................................................
// adds a matrix to a sopiVar.
function newVar = %s_a_sopiVar(b,var)
   newVar = var + b;
endfunction

// sopiVar' ....................................................................
// transposes a sopiVar.
function newVar = %sopiVar_t(var)
   if isscalar(var) then
      newVar = var;
   elseif var.child.operator == TRANSPOSE then
      newVar = var.child.arg(1);
   else
      sopi_loadConstants();
      newVar                  = sopi_newVar();
      newVar.size             = [var.size(2), var.size(1)];
      newVar.child.operator   = TRANSPOSE;
      newVar.child.arg(1)     = var;
      newVar.class            = var.class;      
   end
endfunction

// sopiVar * sopiVar ...........................................................
// performs the multiplication between two sopiVar.
function newVar = %sopiVar_m_sopiVar(var1,var2)
   error("not yet supported")
   // checking sizes
   newSize                 = sopi_checkSizesCoherence(var1,var2,TIMES);
   newVar                  = sopi_newVar();
   newVar.size             = newSize;
   newVar.child            = sopi_newTree();
   newVar.child.operator   = TIMES;
   newVar.child.arg(1)     = var1;
   newVar.child.arg(2)     = var2;
   newVar.class            = sopi_applyMultiplicationRules(var1,var2,newSize);
endfunction

// sopi_applyMultiplicationRules ...............................................
// attempts to determine the curvature of the function given by the multiplication
// of two sopiVar.
function c = sopi_applyMultiplicationRules(var1,var2,s)
   sopi_loadConstants()
   if var1.isLinear & var2.isLinear then
      H = sopi_parseQuadraticTerm(var1,var2);
      t = sopi_testMatrixPositivity(H);
      if t == POSITIVE_DEF | t == POSITIVE_SEMIDEF then
         class = CONVEX;
      elseif t == NEGATIVE_DEF | t == NEGATIVE_SEMIDEF then
         class = CONCAVE;
      else
         class = NONLINEAR;
      end
   else
      // TODO : il y a plein de cas où ca n'est pas vrai : x^2 * x^2 = x^4
      class = NONLINEAR;
   end
   c = sopi_newFunClass(class,s);
endfunction

function [H,c,r] = sopi_parseQuadraticTerm(var1,var2)
   
endfunction

// ============================================================================
// SOPIVAR TRANSFORMATION
// ============================================================================

// sopi_varToFun ...............................................................
// is meant to transform a sopiVar into a callable function to be used in 
// non-linear optimisation.
function fun = sopi_varToFun(var)
   
endfunction
